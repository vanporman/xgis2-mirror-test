# serve files
FROM    nginx:stable
RUN     apt-get update && apt-get install -y curl && apt-get clean
COPY    www/ /usr/share/nginx/html/www
COPY    config/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE  80
CMD     ["nginx", "-g", "daemon off;"]